#!/bin/sh

# File: skeleton-docker/scripts/start_api.sh
#
# When running the Docker container, just do:
#
#     $ docker run -p 8081:8081 \
#              -e DB_URL=mongodb://172.17.0.1:27017/skeleton \
#              avcompris/kalisio-skeleton ./start_api.sh
#

set -e

echo "NVM_DIR: ${NVM_DIR}"

. "${NVM_DIR}/nvm.sh"

cd /home/develop/skeleton/api

yarn dev
