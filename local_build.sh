#!/bin/sh

# File: skeleton-docker/local_build.sh
#
# Use this script to build the Docker image locally.
#
# Note: The buildinfo file will not be filled, nor the image pushed.

set -e

date > buildinfo
echo "HOSTNAME: ${HOSTNAME}" >> buildinfo
echo "USER: ${USER}" >> buildinfo

docker build -t avcompris/kalisio-skeleton .

echo "Done."
