# skeleton-docker

We prepare a fully functional Docker image
with the Kalisio’s skeleton app.


The image is « avcompris/kalisio-skeleton ».

For more info about the Kalisio’s skeleton app,
see [https://kalisio.github.io/skeleton/](https://kalisio.github.io/skeleton/)

## How to build the image

You need Docker.

### Locally

Use [`local_build.sh`](./local_build.sh)

### CI

See [`.gitlab-ci.yml`](./.gitlab-ci.yml)

When run by Jenkis or GitLab-CI, a `buildinfo`
file is added to the image with such info as:

* `GIT_COMMIT` / `CI_COMMIT_SHA`
* `JOB_NAME` / `CI_PROJECT_NAME`
* `BUILD_NUMBER` / `CI_JOB_ID`
* `BUILD_TIMESTAMP`
* `NODE_NAME` / `CI_RUNNER_ID`

This allows to retrieve those info by inspecting
the image, or with the following command:

````
$ docker run avcompris/kalisio-skeleton cat /buildinfo
````

Example of output:

````
CI_COMMIT_SHA: 6f945df569a8e83fb16021251ecec4b2f9f6256c
CI_PROJECT_NAME: skeleton-docker
CI_JOB_ID: 6510960831
BUILD_TIMESTAMP: Fri Mar 29 13:48:04 UTC 2024
CI_RUNNER_ID: 23203354
````

## How to run the image

Typical usage:

````
$ docker run \
    -p 8081:8081 \
    -e DB_URL=mongodb://172.17.0.1:27017/skeleton \
    avcompris/kalisio-skeleton ./start_api.sh
````

and:

````
$ docker run \
    -p 8080:8080 \
    --network=host \
    -e PORT=8081 \
    avcompris/kalisio-skeleton ./start_frontend.sh
````
