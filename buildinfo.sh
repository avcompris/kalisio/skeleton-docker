#!/bin/sh

# File: skeleton-docker/buildinfo.sh
#
# THIS SCRIPT SHOULD BE RUN BY JENKINS OR GITLAB_CI_CD ONLY
#
# Write a "buildinfo" file with all environment information about the
# Jenkins build that lead to the Docker image.
# The resulting "buildinfo" file should be added to the image via the
# Dockerfile, and made available for further auditing.

set -e

log_envvar() {

    VARNAME_JENKINS="${1}"
    VARNAME_GITLAB="${2}"
    VARVALUE_JENKINS=`eval echo "\\$${VARNAME_JENKINS}"`
    VARVALUE_GITLAB=`eval echo "\\$${VARNAME_GITLAB}"`

	if [ -n "${VARVALUE_JENKINS}" ]; then

		echo "${VARNAME_JENKINS}: ${VARVALUE_JENKINS}" | tee -a buildinfo

	elif [ -n "${VARVALUE_GITLAB}" ]; then

		echo "${VARNAME_GITLAB}: ${VARVALUE_GITLAB}" | tee -a buildinfo

	else

        echo "${VARNAME_JENKINS} (Jenkins) or ${VARNAME_GITLAB} (GitLab CI/CD) should be set." >&2
        echo "Exiting." >&2
        exit 1

    fi
}

touch buildinfo

truncate -s 0 buildinfo

if [ -z "${BUILD_TIMESTAMP}" ]; then
	BUILD_TIMESTAMP=`date`
fi

log_envvar GIT_COMMIT       CI_COMMIT_SHA
log_envvar JOB_NAME         CI_PROJECT_NAME
log_envvar BUILD_NUMBER     CI_JOB_ID
log_envvar BUILD_TIMESTAMP  BUILD_TIMESTAMP
log_envvar NODE_NAME        CI_RUNNER_ID
